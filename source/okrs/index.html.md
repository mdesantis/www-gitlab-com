---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](https://about.gitlab.com/strategy/). To make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal). The OKRs are our quarterly goals

## Format

Before the quarter:

`Owner: Key Result as a sentence. Metric`

During and after the quarter:

`Owner: Key Result as a sentence. Metric => Result`

- Owner is the title of person that will own the result.
- We use two spaces to indent instead of tabs.
- OKRs start with the owner of the key result. When referring to a team lead we don't write 'lead' since it is shorter and the team goal is the same.
- The key result can link to an issue.
- The metric can link to real time data about the current state.
- The three company/CEO objectives are level 3 headers to provide some visual separation.

## Levels

We only list your key results, these have your (team) name on them.
Your objectives are the key results under which your key results are nested, these should normally be owned by your manager.
We do OKRs up to the team or director level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Part of the individual performance review is the answer to: how much did this person contribute to the team key objects?
We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Because we go no further than the team level we end up with a maximum 4 layers of indentation on this page (this layer count excludes the three company/CEO objectives).
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every owner should have at most 9 key results. To make counting easier always mention the owner with a trailing colon, like `Owner:`.
The advantage of this format is that the OKRs of the whole company will fit on 3 pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update: make a merge request and assign it to the CEO.
If you're a [team member](https://about.gitlab.com/team/) or in the [core team](https://about.gitlab.com/core-team/) please post a link to the MR in the #okrs channel and at-mention the CEO.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first three weeks of the quarter
1. Review previous quarter and next during board meeting: after the start of the quarter

## Scoring

It's important to score OKRs after the quarter ends to make sure we celebrate what went well, and learn from what didn't in order to set more effective goals and/or execute better next quarter.

1. Move the current OKRs on this page to an archive page _e.g._ [2017 Q3 OKRs](/okrs/2017-q3/)
1. Add in-line comments for each key result briefly summarizing how much was achieved _e.g._
  * "=> Done"
  * "=> 30% complete"
1. Add a section to the archived page entitled "Retrospective"
1. OKR owners should add a subsection for their role outlining...
  * GOOD
  * BAD
  * TRY
1. Promote the draft OKRs on this page to be the current OKRs

## Critical acclaim

Spontaneous chat messages from team members after introducing this format:

> As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up

> I like it too, especially the fact that it is in one page, and that it stops at the team level.

> I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.

> I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."

> I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## Hiring as an objective

The complete hiring plan is kept in the Hiring Forecast doc. Hiring is not an objective in-and-of-itself. However hiring critical members for a team can be considered a key result. This is because recruiting top technical talent in a competitive startup environment can consume a large proportion of management's time and those hires are modeled into our product development goals. Keep all hiring-related KR's in the 'Team' objective.

## Current

These are the OKRs for 2018 Q1.

### Objective 1: Grow Incremental ACV according to plan

* CEO: IACV doubles year over year
  * Sales: Identify success factors
  * Sales: Do quarterly business reviews for all eligible customers
* CEO: Be at a sales efficiency of 1.0 or higher
  * Marketing: know cost per SQO and customer for each of our campaigns
* CEO: Make sure that 70% of salespeople are at 70% of quota
  * Marketing: Make sure each SAL has 10 leads per month
  * Sales: 1 month boot-camp for sales people with rigorous testing
* Support: 100% Premium and Ultimate SLA achievement
* Legal: Implement improved contract flow process for sales assisted opportunities
* Controller: Billing support added for EMEA region.
* Legal: GDPR policy fully implemented.

### Objective 2: Popular next generation product

* CEO: GitLab.com ready for mission critical workloads
  * VPE: Move GitLab.com to GKE
    * Geo: Make Geo performant to work at GitLab.com scale
    * Build: TBD?
    * Gitaly: TBD?
    * CI/CD: TBD?
  * VPE: GitLab.com available 99.95% and monthly disaster recovery exercises
  * VPE: GitLab.com speed index < 1.5s for all tested pages
* CEO: On track to deliver all features of [complete DevOps](https://about.gitlab.com/2017/10/11/from-dev-to-devops/)
  * VPE: Ship faster than before
  * Product: Plan all features to be done by July 22
  * VPE: [One codebase with /ee subdirectory](https://gitlab.com/gitlab-org/gitlab-ee/issues/2952)
* CEO: Make it popular
  * Marketing: Get unique contributors per release to 100
  * Marketing: Increase total users by 5% per month
  * Marketing: Facilitate 100 ambassador events (meetups, presentations)
  * Marketing: Be a leader in all relevant analyst reports
  * VPE: Use all of GitLab ourselves (monitoring, release management)
  * Director of Backend
    * Ensure SP1/SP2 issues for top tier customer get fixed
  * UX
    * UX: [Reduce the installation time of devops for Kubernetes by 50%](https://gitlab.com/groups/gitlab-org/-/epics/33) 
    * UX: [Establish Operation Engineers as a first class citizen. Create a roadmap for Operations to use gitlab as part of their core stack on a day to day basis.](https://gitlab.com/groups/gitlab-org/-/epics/47)
    * UX: [Complete design pattern library, setting usability standards and solutions for design, development, and product management to implement and follow.](https://gitlab.com/groups/gitlab-org/-/epics/29)
  * Geo
    * Geo: Make [Geo Disaster Recovery](https://gitlab.com/gitlab-org/gitlab-ee/issues/846) Generally Available
    * Geo: Reduce bug backlog to 0
    * Geo: Bugs squashed in month _M_ &ge; bugs reported in month _M-1_
    * Geo: Full project-standard unit test coverage for Geo functionality
    * Geo: GitLab-QA tests cover all basic Geo functionality as described in GitLab EE documentation
    * Geo: Deliver 100% of feature commits in 10.5, 10.6
  * Build
    * Build: Upgrade omnibus and internal omnibus-gitlab Chef
    * Build: Measure upgrade/installation time between two GitLab versions. Collect information on user installation methods.
    * Build: Establish a roadmap for automated vulnerability reporting of shipped libraries
    * Build: Cloud Native Helm charts in Alpha
    * Build: Ship 100% of committed deliverables issues each release
  * Platform
    * Platform: Make sure all [Platform backend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=Platform) are merged, closed, labeled “awaiting feedback”, or taken over by us and in active development
    * Platform: Resolve all [Security SL1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Platform&label_name[]=backend&label_name[]=SL1), [Support SP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Platform&label_name[]=backend&label_name[]=SP1), and [Availability AP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Platform&label_name[]=backend&label_name[]=AP1) issues
    * Platform: Close 60 [Platform backend bug](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Platform&label_name%5B%5D=backend&label_name%5B%5D=bug&scope=all&sort=updated_desc&state=opened) issues. Afterwards, we should verify that the backlog went _down_ from the 400 we started with, because otherwise bugs are getting reported faster than we can fix them, and we are not making a dent.
    * Platform: Ship 100% of committed [deliverable issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Platform&label_name[]=backend&label_name[]=Deliverable) each release
    * Platform: Add [backup/restore integration tests](https://gitlab.com/gitlab-org/gitlab-qa/issues/22) to GitLab QA
    * Platform: Ship [first GraphQL endpoint](https://gitlab.com/gitlab-org/gitlab-ce/issues/34754)
  * Discussion
    * Discussion: Make sure all [Discussion backend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=Discussion) are merged, closed, labeled “awaiting feedback”, or taken over by us and in active development
    * Discussion: Resolve all [Security SL1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=SL1), [Support SP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=SP1), and [Availability AP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=AP1) issues
    * Discussion: Close 36 [Discussion backend bug](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Discussion&label_name%5B%5D=backend&label_name%5B%5D=bug&scope=all&sort=updated_desc&state=opened) issues. Afterwards, we should verify that the backlog went _down_ from the 280 we started with, because otherwise bugs are getting reported faster than we can fix them, and we are not making a dent.
    * Discussion: Ship 100% of committed [deliverable issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=Deliverable) each release
    * Discussion: Ship Rails 5 migration
  * CI/CD
    * CI/CD: Resolve or schedule all AP1, AP2, SP1, and at least 5 SP2 issues
    * CI/CD: Scalability: Make all CI/CD related data to be stored on Object Storage
    * CI/CD: Performance: Ensure that 95% of pipeline jobs are picked within the first 10s for projects currently not having any jobs running
    * CI/CD: Cost: Move all CI infrastructure to GCP
    * CI/CD: Quality: Test CI workflow with Runner by GitLab QA
    * CI/CD: Ship 100% of committed deliverables issues each release
  * Monitoring
    * Monitoring: Bundle Alertmanager for proactive customer alerting notifications
    * Monitoring: Prometheus deploy for customer apps feature
    * Monitoring: Ship 10 new alerts for monitoring GitLab
    * Monitoring: Instrument gitlab-shell
    * Monitoring: Ship 100% of committed deliverables issues each release
  * Security
    * Security: GDPR: Complete data breach notification policy and data mapping requirements (Compliance-KW)
    * Security: FIPS 140-2: Research requirements and provide guidance to development team to implement (Compliance-KW)
    * Security: SOC 2: Research requirements and build initial roadmap to achieve compliance (Compliance-KW)
    * Security: Complete Remainder of 10 Risk Assessment Actions (Abuse-KW/JT/JR)
    * Security: Automate metrics for vulnerability initiatives: HackerOne, external & internal assessments (Automation-JT)
    * Security: Conduct 2 product application security reviews (AppSec-JR)
    * Security: Manage Advance Notification Program for security releases (SecOps-KW/JT/JR)
    * Security: All parts of security active (AppSec-JR, Automation-JT, SecOps-KW/JT/JR, Abuse-KW/JT/JR, Compliance-KW)
  * Database
    * Database: [Make it more difficult for database performance issues to occur](https://gitlab.com/gitlab-com/infrastructure/issues/3474)
    * Database: [Improve workflow / structure of the database team](https://gitlab.com/gitlab-com/infrastructure/issues/3475)
    * Database: [Improve database performance](https://gitlab.com/gitlab-com/infrastructure/issues/3476)
  * Gitaly
    * Gitaly: Deliver 100% of committed scope for GCP migration milestone [#2](https://gitlab.com/gitlab-com/migration/milestones/2) by Jan 15
    * Gitaly: Deliver 100% of committed scope for GCP migration milestone #3 by Feb 15
    * Gitaly: Deliver 100% of committed scope for GCP migration milestone #4 by Mar 15
    * Gitaly: All migration points complete to Ready-for-Testing state by 7 February
    * Gitaly: All endpoints complete to Opt-In state (including ones currently disabled due to n+1)
    * Gitaly: Hit 1.0 (all endpoints complete to Opt-Out state)
  * Quality
    * Quality: Complete the work to [make GitLab QA production-ready](https://gitlab.com/gitlab-org/gitlab-qa/issues/126) => Done.
    * Quality: [Define the architecture of and produce an end-to-end prototype for a self-service metrics generator](https://gitlab.com/gitlab-org/gitlab-insights/issues/2)
    * Quality: Define and schedule high-value issues for [improving the staging test environment](https://gitlab.com/gitlab-com/infrastructure/issues/3177)
    * Quality: [Write 2 GitLab QA tests related to creating and managing Issues](https://gitlab.com/groups/gitlab-org/-/epics/44)
    * Quality: [Write 2 GitLab QA tests related to CI/CD](https://gitlab.com/groups/gitlab-org/-/epics/45)
  * Edge
    * Edge: [Work with backend teams to move 100% of EE-specific **files** and 50% of EE-specific **lines of code** to the top-level `/ee` directory](https://gitlab.com/groups/gitlab-org/-/epics/27)
    * Edge: [Investigate how to extract EE-specific files/lines of code for JavaScript, CSS, and Grape API](https://gitlab.com/gitlab-org/gitlab-ee/issues/4643)
    * Edge: [Reduce average CE pipeline duration to 30 minutes](https://gitlab.com/gitlab-org/gitlab-ce/issues/41726)
    * Edge: Solve at least 1 [outstanding performance issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance&milestone_title=No+Milestone)
  * Frontend
    * Frontend: Write 200 unit tests to resolve test debt
    * Frontend: Crush 300 backlogged bugs
    * Frontend: Ship 100% of committed deliverables issues each release
    * Frontend: Make sure all [Frontend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=frontend) are merged, closed, labeled “awaiting feedback”, or taken over by us an in active development
    * Frontend: Close our main JS technical debt topics: Library updates, Global Code splitting and reduce our bundle size significantly per page
    * Frontend: Set up site speed docker container in our CI, running automated daily, and pushing stats to existing Grafana instance
  * Data and Analytics: Create the execution plan for the data enabled user journey.

### Objective 3: Great team

* CEO: Hire according to plan
* CEO: Great and diverse hires
  * Global hiring
  * Sourced recruiting 50% of applicants
  * Hired candidates, on average, from areas with a [Rent Index](https://about.gitlab.com/handbook/people-operations/global-compensation/#the-formula) of less than 0.7
* CEO: Keep the handbook up-to-date so we can scale further
  * Handbook first (no presentations about evergreen content)
  * Consolidate and standardize role descriptions
* VPE: Consolidate and standardize job descriptions
* VPE: Launch 2018 Q2 department OKRs before EOQ1 2018
* VPE: Set 2018 Q2 hiring plan before EOQ1 2018
* VPE: Implement issue taxonomy changes to improve prioritization
* VPE: Record an on-boarding video of how to do a local build and contribute to the GitLab handbook
* Backend: Deliver two iterations toward aligning backend teams with the DevOps lifecycle
* Support
  * Support: Define HA Expertise and train 7 support engineers
  * Support: Define Kubernetes Expertise and train 7 support engineers
* UX
  * UX: [Deprecate outdated UX Guide and replace with design.gitlab.com to communicate current UX standards and solutions across teams.](https://gitlab.com/groups/gitlab-org/-/epics/30)
  * UX: [Write 3 public blog posts about GitLab UX and visual design case studies, best practices, anecdotes, or events](https://gitlab.com/groups/gitlab-org/-/epics/33)
* Quality: Document the context and background of release process improvements in the Handbook / Quality page
* Frontend: Establish and shape our Frontend specific on-boarding
* Data and Analytics: Corporate dashboard in place for 100% of company metrics.
* Controller: ASC 606 implemented for 2017 revenue recognition
* Billing Specialist: Add cash collection, application and compensation to job responsibilities.
* Controller: Close cycle reduced to 9 days.
* Accounting Manager: All accounting policies needed to be in place for audit are documented in the handbook.
* Legal: Add at least one country in which headcount can be grown at scale.
* VPE: Hire a Director of Engineering
* VPE: Hire a Director of Infrastructure
* VPE: Hire a Database Manager
* VPE: Hire a Production Engineer
* Build: Hire a Build Engineer
* Discussion: Hire two developers
* Quality: Hire an Engineering Manager
* Security: Hire 2 Security Engineer, SecOps
* Platform: Hire 2 developers
* CI/CD: Hire 2 developers

## Archive

* [2017 Q3](/okrs/2017-q3/)
* [2017 Q4](/okrs/2017-q4/)
