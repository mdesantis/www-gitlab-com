---
layout: job_page
title: "Chief Operating Officer (COO)"
---

DRAFT

The chief operating officer (COO) is responsible for using data to make GitLab's customers more effective.

## Vision

1. Software is eating the world.
1. Every company needs to become an effective software company to survive.
1. Effective software companies have a short time to value.
1. GitLab is the only integrated product that can help you reduce time to value.
1. This will be a long journey and we will partner with our customers to achieve it.
1. Together devise a plan to improve the time to value and we provide content and services.
1. Organizes around the world share the best software development practices in GitLab.
1. GitLab partners with every large organization to reduce time to value.

## Customer perspective

Right now every large enterprise is suffering from a lack of consistency:

1. Not using the same tools
1. Not using the same integrations
1. Not using the same configuration
1. Not using the same work processes
1. Not being judged on the same metrics

And they have processes the block reducing time to value, for example:

1. Security reviews that are blocking until approved
1. Infrastructure that has to be provisioned
1. Fixed release windows
1. Production that needs to approve releases
1. SOX compliance sign offs that need to happen
1. QA cycles
1. Separate QA teams
1. Separate build teams

## Solution

Use data to communicate the same message to the customer throughout all communication from GitLab as a company and product:

- Customer Success (SAs game plans)
- Sales (auto generated presentations)
- Support (intake)
- Marketing (case studies, ROI calculator, web pages, whitepapers, analysts)
- DevRel (meetups, blog posts
- Product start (Auto DevOps)
- Product hints (you want to use)
- Product interface (where you are in the process)
- Product analytics (ConvDev index)
- Analytics (warehouse and usage ping)

## Responsibilities

The COO ensures that we that we capturing relevant data, perform analysis, distill insights, and implementing actions based on them.

- Customer success
- Pricing
- Cross department definitions (what is a customer, subscription, sale stage, etc.)
- Data warehouse/data collection
- Analytics/data transformation
- [Company dashboard](https://about.gitlab.com/handbook/finance/analytics/)
- [Sales enablement](https://gitlab.com/gitlab-com/organization/issues/95)
- Cross functional automation (marketing, sales, billing)
- Scaling the company as it grows

The analytics effort is cross-functional:

- Product => what features do people want to pay for?
- Engineering => how do we instrument our product and services?
- Product marketing => what is the most effective message?
- Lead generation => what channels are most effect?
- Initial sale => what free users are most likely to buy?
- Customer Success => who do we message what to increase adoption?

## Sales organization

- Also see our [market segmentation](https://about.gitlab.com/handbook/sales/#market-segmentation).
- Large new accounts are handled by account executives and solution architects (pre-sales engineers).
- Small new accounts are handled by our BDR team.
- Large existing accounts are handled by account managers and customer success managers (CSMs).
- Small existing accounts are not touched personally.
- Most people above report to the CRO, the BDRs report to marketing, the TAMs report to the COO.

## Reports

- Director of the customer success managers
- Data infrastructure/engineering (includes product data, market and business data)
- Analytics (product, marketing, and sales analytics, partner with leadership, more than business intelligence)
- Data science (machine learning, coding, AI, part digital sales/gtm, part growth team on product side)
- Go to market operations (sales, marketing, and customer success automation)
- VP of Scaling (moving more towards data science)
- Professional Services (part deploy and conversion, part SDLC workflow)
- The COO owns the user journey. Although product, marketing, and sales don't report up to the COO they are accountable in that regard.

## Requirements

* Experience successfully leading a comparable effort
* Result focus
* Demonstrated use of analytics to double down on successful efforts
* Outstanding written communication skills
* Desire to develop a team
* Ability to partner with other executive team members
* Successful completion of a [background check](/handbook/people-operations/#background-checks).

## To Apply

We're not recruiting yet for this position.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

