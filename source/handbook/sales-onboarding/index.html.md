---
layout: markdown_page
title: "Sales Onboarding"
---

This page has been deprecated and added to the [People Ops onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md.
