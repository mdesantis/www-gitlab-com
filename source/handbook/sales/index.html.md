---
layout: markdown_page
title: "Sales Handbook"
---

## Reaching the Sales Team (internally)

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/sales/issues/); please use confidential issues for topics that should only be visible to team members at GitLab.
- Please use the 'Email, Slack, and GitLab Groups and Aliases' document for the appropriate alias.
- [**Chat channel**](https://gitlab.slack.com/archives/sales); please use the `#sales` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

---
## On this page
{:.no_toc}

- TOC
{:toc}

---

## Other Sales Topics in Handbook

* [Sales Onboarding](/handbook/sales-onboarding/)
* [Sales Standard Operating Procedures](/handbook/sales/sop)
* [Business Operations](/handbook/business-ops)
* [Sales Skills Best Practices](/handbook/sales-training/)
* [Sales Discovery Questions](/handbook/sales-qualification-questions/)
* [EE Product Qualification Questions](/handbook/EE-Product-Qualification-Questions/)
* [FAQ from prospects](/handbook/sales-faq-from-prospects/)
* [How to engage a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect)
* [How to initiate a refund](/handbook/finance/Accounts-receivable-and-cash/#sts=Refunds)
* [Client Use Cases](/handbook/use-cases/)
* [POC Template](/handbook/sales/POC/) to be used to help manage a trial/proof of concept with success criteria
* [Account Planning Template for Large/Strategic Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24)
* [Demo](/handbook/sales/demo/)
* [Dealing with security questions from prospects](/handbook/engineering/security/#security-questionnaires-for-customers)
* [Sales Development Team Handbook](/handbook/marketing/marketing-sales-development/sdr/) - moved to Marketing & Sales Development section
* [Who to go to to ask Questions or Give Feedback on a GitLab feature](/handbook/product/#who-to-talk-to-for-what)
* [CEO Preferences when speaking with prospects and customers](/handbook/people-operations/ceo-preferences/#sales-meetings)


### Sales Resources outside of the Sales Handbook

* [Resellers Handbook](/handbook/resellers/)
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Sales Collateral](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)
* [Sales Training](https://drive.google.com/open?id=0B41DBToSSIG_WU9LZ0o0ektEd0E)
* [Lead Gen Content Resources](https://about.gitlab.com/resources/)
* [GitLab ROI Calculator](https://about.gitlab.com/roi/?team_size=100&developer_cost=75)
* [GitLab University](https://docs.gitlab.com/ce/university/)
* [GitLab Support Handbook](/handbook/support/)
* [GitLab Hosted](https://about.gitlab.com/gitlab-hosted/)

### GitLab Tech Stack

For information regarding the tech stack at GitLab, please visit the [Business Operations section of the handbook](/handbook/business-ops#tech-stack) where we maintain a comprehensive table of the tools used across the Marketing, Sales and Customer Success functional groups, in addition to a 'cheat-sheet' for quick reference of who should have access and whom to contact with questions.


### Sales Team Meetings and Presentations

#### Sales Team Call

* General Guidelines
  1. We start on time and do not wait for people.
  1. If you are unable to attend, just add your name to the list of "Not Attending".
  1. When someone passes the call to you, no need to ask "Can you hear me?" Just begin talking and if you can't be heard, someone will let you know.
* The Sales team call is every Monday 9:00am to 9:30am Pacific Time. The call is cancelled on US holidays that fall on Mondays.
* The agenda can be found here in Google Drive: [Sales Team Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit). Remember that everyone is free to add topics to the agenda. Please start with your name and be sure to link to an issue, merge request or commit if that is relevant.
* We use [Zoom](https://zoom.us) for the call since Hangouts is capped at 15 people, link is in the calendar invite, and also listed at the top of the Sales Team Agenda.
* The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos". There is a subfolder called [Sales Team Meeting](https://drive.google.com/drive/u/0/folders/0B5OISI5eJZ-DTndfelNnbXViUjA), which is accessible to all users with a GitLab.com e-mail account.
* The general order of the call is outlined below. Once the presenter has shared all of his/her items, he/she should then ask for questions. If there are no questions or all the questions have been answered, the presenter is to handoff to the next presenter on the agenda.
   1. The CRO will start the call with performance to-date for the month/quarter, then share the remaining forecast for the month/quarter. The CRO will then discuss any number of  topics ranging from pipeline cleanup, process reminders, and other  messages to the team. If the CRO is not attending, the Director of Sales Operations will share this update.
   1. The Director of Sales Operations will provide an update to process changes, new fields, and other updates related to sales process and team productivity.
   1. The Marketing Team will provide an update to the Sales Team. The SDR Team Manager will provide an update on SDR and BDR team performance to plan: pipeline created, meetings scheduled, and other SDR/BDR team metrics. Other members of the marketing team may provide updates- new collateral, events, and other marketing-related activities.
   1. The Customer Success Team is next. The Director of Customer Success may provide an update to an existing process or an introduction to a new process. In some cases, Solutions Architects may provide an update as well.
   1. Next, any topics from the team for discussion are encouraged. This can include questions on products, processes, or reminders to the rest of the team. In some cases, a summary of an event (client meetings, meetups, trade shows) will be shared.
   1. The call will conclude with a general 'scrum', which is designed for Account Executives and Account Managers to share anything new they have learned over the last week. This may include some competitive knowledge, new-found strategies or tactics that they have found helpful.
* If you cannot join the call, consider reviewing the recorded call or at minimum read through the sales team agenda and the links from there.

#### Sales Brown Bag Training Call

* When: Happens every Thursday at Noon Eastern Time
* Where: https://gitlab.zoom.us/j/354866561
* What: [Agenda](https://docs.google.com/document/d/1ygbqKyjxJvD195MTtDo53WlCk4idVQHZta3Onmh2vAc/edit)

#### Sales Team Functional Update

* When: Happens once every 4 to 5 weeks at 11 AM Eastern Time
* Where: Zoom link varies- please be sure to check the calendar invitation
* What: The CRO and Director of Sales Operations will provide the company with an update on sale team performance, notable wins, accomplishments, and strategies planned. You can view previous updates in this [Sales Functional Update Google Drive Folder](https://drive.google.com/drive/folders/0B6KxbJ3HZq2YY3Nfa1o4T0lKQVk?ths=true)

### Collaboration Applications used by Sales

To ensure everyone can collaborate and contribute, sales uses [Google applications](https://www.google.com/sheets/about/).  We use Google Sheets instead of Excel.  We use Googe Docs instead of Word.  We use Google Slides instead of Keynote or Powerpoint.

You can save your work and find work created by others in our [Google Sales Drive](https://drive.google.com/drive/u/0/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM)


### Pitches

#### Elevator pitch

The Problem - Customer perspective

Right now every large enterprise is suffering from a lack of consistency:

* Not using the same tools
* Not using the same integrations
* Not using the same configuration
* Not using the same work processes
* Not being judged on the same metrics

And they have processes which block reducing time to value, for example:

* Security reviews that are blocking until approved
* Infrastructure that has to be provisioned
* Fixed release windows
* Production that needs to approve releases
* SOX compliance sign offs that need to happen
* QA cycles
* Separate QA teams
* Separate build teams

Solution

GitLab is the only integrated product that can help you reduce time to value.

GitLab is an integrated product for the entire software development lifecycle. It contains chat, issue tracking, kanban boards, version control, continuous integration, release automation, and monitoring. 2/3 of the organizations that self-host git use GitLab, that are more than 100,000 organizations and millions of users.

#### Is it similar to GitHub?

GitLab started as an open source alternative to GitHub.
Instead of focusing on hosting open source projects we focused on the needs of the enterprise, for example we have 5 authorization levels vs. 2 in GitHub.
Now we've expanded the feature set with continuous integration, continuous delivery, and monitoring.

#### What is the benefit?

We help organizations go faster to market by reducing the cycle time and simplifying the toolchain development and operations use to push their software to market.
Before GitLab you needed 7 tools, it took a lot of effort to [integrate](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) them, and you end up with have different setups for different teams.
With GitLab you gain visibility on how long each part of the software development lifecycle is taking and how you can improve it.

#### Email intro

GitLab makes it easier for companies to achieve software excellence so that they can unlock great software for their customers by reducing the cycle time between having an idea and seeing it in production. GitLab does this by having an [integrated product](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) for the entire software development lifecycle. It contains not only issue management, version control and code review but also Continuous Integration, Continuous Delivery, and monitoring. Organizations all around the world, big and small, are using GitLab. In fact, 2/3 of organizations that self-host git use GitLab. That is more than 100,000 organizations and millions of users.

#### [GitLab Positioning](/handbook/positioning-faq/)

### Market segmentation

#### Organization size

Our market segmentation for organizations is based on the people in a function that can use an application like GitLab:

1. Strategic: 5000+ IT + TEDD employees
2. Large: 750 - 4999 IT + TEDD employees
3. Mid Market: 100 - 749 IT + TEDD employees
4. SMB: less than 100 IT + TEDD employees (SMB stands for small and medium businesses)

#### Initial owner

The people working with each segment and their quota are:

1. Strategic & Large: Strategic Account Leader (SAL) with a quota of $1.25M incremental annual contract value (IACV) per year
1. Mid market: Account Executive with a quota of $750,000 incremental ACV (IACV) per year
1. SMB: SMB Customer Advocate can close SMB deals directly by referring them to our self-serve web store or assisting them with a direct purchase.

Quotas may be adjusted based on geographic region.

Variable Compensation takes the following into consideration:

* 75% of variable - Incremental ACV
* 5% of variable - Renewal ACV
* 20% of variable - Multi-year prepaid deals and professional services revenue.

#### Quota Ramp

For all quota carrying salespeople, we expect them to be producing at full capacity after 6 full months.  We have the following ramp up schedule and measure perforamnce based on this schedule:

* Month 1 - 0%
* Month 2 - 0%
* Month 3 - 25%
* Month 4 - 50%
* Month 5 - 50%
* Month 6 - 75%
* Month 7+ - 100%

For the purposes of our internal analytics tool, InsightSquared, we have the first 3 months at 25%.  We do this so we do not have overinflated %'s when a salesperson does sell something in months 1 and 2.'

#### Customer Success

See our [customer success page in the handbook for more details](/handbook/customer-success/) but the summary is:

1. Strategic and Large: the Strategic Account Leader stays with account, they can rely on [Solution Architects](/handbook/customer-success/) pre-sale and Technical Account Managers (TAM) post-sale (TAM is only for strategic accounts, not for large ones).
1. Mid market: Directly after the initial sale this is no longer owned by the account executive but by our account managers.
1. SMB: There are no dedicated customer success people.


#### How Sales Segments Are Determined

Sales Segmentation information can be found in the [Business Operations - Database Management](/handbook/business-ops/database-management#segmentation) section.


### GitLab Usage Statistics

Using [GitLab Version Check](/handbook/sales-process/version_check), GitLab usage data is pushed into Salesforce for both CE, EE and EE trial users. Once in Salesforce application, you will see a tab called "Usage Statistics".  Using the drop down view, you can select CE, EE Trails or EE to see all usage data sent to Gitlab.
Since version check is pulling the host name, the lead will be recorded as the host name.  Once you have identifed the correct company name, please update the company name. Example: change gitlab.boeing.com to Boeing as the company name.

To view the usage, click the hyperlink under the column called "Usage Statistics".  The link will consist of several numbers and letters like, a3p61012001a002.
You can also access usage statistics on the account object level within Salesforce.  Within the account object, there is a section called "Usage Ping Data", which will contain the same hyperlink as well as a summary of version they are using, active users, historical users, license user count, license utilization % and date data was sent to us.

A few example of how to use Usage Statistics to pull insight and drive action?
* If prospecting into CE users, you can sort by active users to target large CE instances. You can see what they are using within GitLab, like issues, CI build, deploys, merge requests, boards, etc.  You can then target your communications to learn how they are using GitLab internally and educate them on what is possible with EE.
* For current EE users who are below their license utilization, you can engage the customer to understand their plan to rollout GitLab internally and how/where we can help them with adoption.
* For current EE users who are above their license utilization, you can leverage the data to engage the customer.  Celebrate the adoption of GitLab within their organization.  Ask why the adoption took off?  How they are using it (use cases)? Engage them in amending their contract right now for the add-on? Update the renewal opportunity to reflect the increase in usage for the true-up and new renewal amount.
* For current EE users who are not using a EE feature, i.e. CI or issues, you can engage the customer to understand why they are not using it.  Do they know we offer it? Are they using a competitive tool?  Have the integrated their current tool into GitLab? Are they open to learning more about what we offer to replace their current tool?
* For EE trials.  What EE features are they using and not using? If using a EE feature, what are they trying to solve and evaluate? If not using, why and are they open to evaluating that feature?

Here is a [Training Video](https://drive.google.com/drive/u/0/folders/0B41DBToSSIG_WU9LZ0o0ektEd0E) to explain in greater detail.

### GitLab CE Instances and CE Active Users on SFDC Accounts

In an effort to make the [Usage/Version ping data](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) simpler to use in SFDC, there are 2 fields directly on the account layout - "CE Instances" and "Active CE Users" under the "GitLab/Tech Stack Information" section on the Salesforce account record.

The source of these fields is to use the latest version ping record for and usage record for each host seen in the last 60 days and combine them into a single record. The hosts are separated into domain-based hosts and IP-based hosts. For IP based hosts, we run the IP through a [reverse-DNS lookup](https://en.wikipedia.org/wiki/Reverse_DNS_lookup) to attempt to translate to a domain-based host. If no reverse-DNS lookup (PTR record) is found, we run the IP through a [whois lookup](https://en.wikipedia.org/wiki/WHOIS) and try to extract a company name, for instance, if the IP is part of a company's delegated IP space. We discard all hosts that we are unable to identify because they have a reserved IP address (internal IP space) or are hosted in a cloud platform like GCP, Alibaba Cloud, or AWS as there is no way to identify those organizations through this data. For domain-based hosts, we use the [Clearbit](https://clearbit.com) domain lookup API to identify the company and firmographic data associated with the organization and finally the [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/start) to parse, clean, and standardize the address data as much as possible.

These stats will be aggregated up and only appear on the Ultimate Parent account in SFDC when there are children (since we don't really know which host goes to which child).

To see a list of accounts by # of CE users or instances, you can use the [CE Users list view](https://na34.salesforce.com/001?fcf=00B61000004XccM) in SFDC. To filter for just your accounts, hit the "edit" button and Filter for "My Accounts". Make sure to save it under a different name so you don't wipe out the original. These fields are also available in SFDC reporting.

A few caveats about this data:
* The hosts are mapped based on organization name, domain, and contact email domains to relate the instances to SFDC accounts as accurately as possible, but we may occasionally create false associations. Let the Data & Analytics team know when you find errors or anomalies.
* Both fields can be blank, but the organization can still be a significant CE user. Both fields being blank just means that there are no instances sending us version or usage pings, not necessarily that there are no active instances.
* Users can be the same person in multiple instances, so if a user exists in several instances at an organization with the usage ping on, they are counted each time they appear in an instance. Users may also be external to an organization, so the number of users may not represent employees and can thus be higher than the number of employees.
* These numbers represent the minimum amount that GitLab is likely being used within the organization, since some instances may have these pings turned on and some off. They also may have just the version ping on, in which case we won't see the number of users. This should not be interpreted as having instances but no users.

For the process of working these accounts, appending contacts from third-party tools, and reaching out, please refer to the [business operations](../business-ops/index.html.md) section of the handbook.

### Requesting assistance/introductions into prospects from our investors

We have great investors who can and want to help us penetrate accounts.  Each month we send out an investor update and within this update there will be a section called "Asks".  Within this section, we can ask investors for any introductions into strategic accounts.
To make a request, within the Account object in Salesforce, use the chatter function and type in "I'd like to ask our investors for help within this account".  Please cc me and your manager within the chatter message.  All requests should be made before the 1st
of the month so they be included in the upcoming investor update.

If an investor can help, they will contact the CRO and the CRO will introduce the salesperson and the investor.  Salesperson shall ask how the investor wants to be updated on progress and follow up accordingly.

### Parent and Child Accounts

* A Parent Account is the business/organization which owns another business/organization.  Example: The Walt Disney Company is the parent account of Disney-ABC Television Group and Disney.com.
* A Child Account is the organization you may have an opportunity with but is owned by the Parent Account. A Child Account can be a business unit, subsidiary, or a satellite office of the Parent Account.
* You may have a opportunity with the Parent account and a Child Account.  Example: Disney and ESPN may both be customers and have opportunities. However, the very first deal with a Parent Account, whether it is with the Parent Account or Child Account, should be marked as "New Business". All other deals under the Parent Account will fall under Add-On Business, Existing Account - Cross-Sell, or Renewal Business (see Opportunity Types section).
* If the Parent and Child accounts have the same company name, either add the division, department, business unit, or location to the end of the account name. For example, Disney would be the name of the Parent Account, but the Child Account would be called The Walt Disney Company Latin America or The Walt Disney Company, Ltd Japan.
* When selling into a new division (which has their own budget, different mailing address, and decision process) create a new account.  This is the Child Account.  For every child account, you must select the parent account by using the parent account field on the account page. If done properly, the Parent/Child relationship will be displayed in the Account Hierarchy section of the account page.
* Remember that a child account can also be a parent account for another account. For example, Disney-ABC Television Group is the child for The Walt Disney Company, but is the parent for ABC Entertainment Group.
* We want to do this as we can keep each opportunity with each child account separate and easily find all accounts and opportunities tied to a parent account, as well as roll-up all Closed Won business against a Parent Account.

### When to Create an Opportunity

Before a lead is converted or an opportunity is created the following must occur:

1. Authority
  * What role does this person play in the decision process? (i.e. decision maker, influencer, etc.) OR is this someone that has a pain and a clear path to the decision maker
1. Need
  *Identified problem GitLab can solve
1. Required Information
  * Number of potential EE users
  * Current technologies and when the contract is up
  * Current business initiatives
1. Handoff
  * The prospect is willing to schedule another meeting with the salesperson to uncover more information and uncover technical requirements and decision criteria
1. Interest by GitLab salesperson to pursue the opportunity.  Are you interested in investing time in pursuing this opportunity.

#### Creating a New Business Opportunity

Since a new opportunity is being created with no previous campaign attribution, we must first associate this opportunity to a lead or contact. This will accomplish two things: first, we will attribute all of the lead or contact's previous campaigns to an opportunity for attribution; and second, the lead or the contact will become the primary contact on the new opportunity.

1. From Lead Conversion: Once you have qualified the prosect via our sales development qualification process (please see the [Sales Development handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/oldBDRHandbook.html#qualifying) for more information), you will convert the lead.

1. From the Contact Record: On the contact page, click on the New Opportunity button. Then enter all required fields, including Opportunity Name, Stage, and Close Date. Then click Save.

#### Creating an Add On Business Opportunity

An add on  opportunity will inherit information (including marketing attribution) from the original new business opportunity, so instead of creating an add on  opportunity from a converted lead or from a contact record, an add on  opportunity is created from the original new busienss opportunity record. This establishes a parent-child relationship between the original new business opportunity and the add on opportunity you are creating.

1. Go to the original new business opportunity that will become the parent opportunity. For example, if you are selling additional seats to an existing subscription, you shoud go to the original new business opportunity.
1. Click on the "New Add On Opportunity" button.
1. Update the Opportunity Name (see the [Opportunity Naming Conventions](https://about.gitlab.com/handbook/sales/#opportunity-naming-convention)).
1. Enter the Lead Source, Close Date, and Stage.
1. If there are other fields you'd like to populate you can do so.
1. Click Save.

Once the parent-child opportunity hierarchy is established, as mentioned earlier some information will pass from the parent (new business) to the child (add on) opportunity. This information will be used in our reporting and analysis to track add on business opportunities to their initial sources and team members.

* Initial Lead Source
* Initial Sales Qualified Source
* Initial Business Development Representative
* Initial Sales Development Representative

Please note the addition of some validation rules with the new marketing attribution requirements:

* The parent opportunity must either be a new business or renewal opportunity. A parent opportunity cannot be another add on opportunity.
* All sales-assisted non-portal add on opportunities must have a parent opportunity.

### Tracking Sales Qualified Source in the Opportunity

Sales Qualified Source is dimension used when analyzing pipeline creation, lead conversion, sales cycles, and conversion rates. Sales Qualified Source may be different from the Lead Source of an Opportunity, which captures the original source (event, campaign, etc). For example, if a prospect originates from a trial (lead source), that prospect can be qualified by a BDR, SDR, Account Executive, Channel Partner, or the Web (sales qualified source).

The logic for the Sales Qualified Source is as follows:

1. If the Business Development Representative field (Opportunity) is populated, regardless of opportunity owner, the Sales Qualified Source is "BDR Generated"
2. If the Sales Development Representative field (Opportunity) is populated, regardless of opportunity owner, the Sales Qualified Source is "SDR Generated"
3. If both the Business Development Representative and Sales Development Representative fields are NULL and the opportunity owner is:
   * a Regional Director, Account Executive, or Account Manager, the Sales Qualified Source is "AE Generated"
   * a GitLab team member that is not a Regional Director, Account Executive, or Account Manager, the Sales Qualified Source is "Other"
   * an authorized reseller, the Sales Qualified Source is "Channel Generated"
   * the Sales Admin, the Sales Qualified Source is "Web Direct Generated"


### Reseller Opportunities

Opportunities utilizing a reseller require slightly different data:

* Opportunity Name:
If the partner is an authorized reseller, rename the opportunity with the partner’s nick-name in front, then a dash.  For instance; if it is a Perforce deal, the opportunity name should start with P4 - (whatever your opportunity name is)  This is important for the workflow that solicits updates from the reseller.

* Account Name:
It is important that opportunities using a reseller are created on the END CUSTOMER’s account, and not the reseller’s account.  The account name on an opportunity is never a reseller.  Resellers do not buy licenses; they purchase them on the behalf of an end customer.  For instance, the account name field on an opportunity should never be SHI.

* Opportunity Owner:
Should be the name of the AE who is working the deal with the reseller

* Associating Contact Roles:
After creating the opportunity, click “New” in the contact section to associate contacts with the opportunity.
 - The primary contact should always be a contact at the end user’s account and not a contact at the reseller.  This is important as resellers come and go, and if we do not capture the contact at the end user account, we will not be able to sell to this account if the reseller ends their relationship with us or with the end account.
 - A reseller contact (say, the sales rep at ReleaseTEAM) can, and should be added to the opportunity with the role of Influencer.  NOTE: A contact that works for a reseller should never be added to an end user account.  For instance an employee of SoftwareOne should be a contact of the SoftwareOne account only, and not the Boeing account.

* Associating Partners to an Opportunity:
After creating the opportunity, click “New” in the Partners section to associate the reseller with the opportunity.
 - You can associate multiple partners with an opportunity if there is more than one reseller involved in the opportunity.  This is not uncommon for government opportunities, or opportunities where the customer is asking multiple fulfillment houses (like SHI and SoftwareOne) to fulfill the order.

### Opportunity Naming Convention

Opportunities for subscriptions will use the following guidelines:

- **New Businessl**: [Quantity]
   - [Name of Company]- [Quantity] [Abbreviations of Product]
   - Example: Acme, Inc- 50 EEP

- **Add-On Business (seats only)**:
   - [Name of Company]- Add [Quantity] [Abbreviations of Product]
   - Example: Acme, Inc- Add 25 EEP

- **Add-On Business (Upgrade from Starter to Premium)**:
   - [Name of Company]- Upgrade to EEP
   - Example: Acme, Inc- Upgrade to EEP

- **Renewal Business (no changes)**:
   - [Name of Company]- [Quantity] [Abbreviations of Product] Renewal [MM/YY]
   - Example: Acme, Inc- 50 EEP Renewal 01/17

- **Renewal Business + Add On Business (seats)**:
   - [Name of Company]- [Quantity] [Abbreviations of Product] Renewal [MM/YY]+ Add [Quantity]
   - Example: Acme, Inc- 50 EEP Renewal 01/17 + Add 25

- **Renewal Business + Upgrade**:
   - [Name of Company]- [Quantity] Upgrade to EEP + Renewal [MM/YY]
   - Example: Acme, Inc- 50 Upgrade to EEP + Renewal 01/17

- **Refunds**:
   - [Original Opportunity Name] - REFUND
   - Example: Acme, Inc- 50 Upgrade to EEP + Renewal 01/17 - REFUND

Abbreviation of Products:

- EES- Enterprise Edition Starter
- EEP- Enterprise Edition Premium

### Opportunity Types

There are three things that can be new or existing:

- Account (organization)
- Subscription (linked to a GitLab instance)
- Amount (dollars paid for the subscription)

That gives 4 types of of opportunities:

1. New account (new account, new subscription, new amount) This type should be used for any new subscription who signs up either through the sales team or via the web portal. Paid training also falls under this type if the organization does not have an enterprise license.
1. New subscription (existing account, new subscription, new amount) If an existing account is purchasing a new license for another GitLab instance, this will be new business.
1. Add-on business (existing account, existing subscription, new amount) This type should be used for any incremental/upsell business sold into an existing subscription division mid term, meaning not at renewal. This may be additional seats for their subscription or an upgrade to their plan. If an existing account is adding a new subscription, this would be new business, not an add-on.
1. Renewal (existing subscription, existing subscription, existing amount) This type should be used for an existing account renewing their license with GitLab. Renewals can have their value increased, decreased, or stay the same.  We capture incremental annual contract value growth/loss as a field in Salesforce.com. Renewal business can be a negative amount if renewed at less than the previous dollars paid for the subscription (renewal rate). Only the part that is more or less than the old amount is IACV, the rest is part of the the renewal opportunity.

**New business** is the combination of new account and new subscription.

### Deal Sizes

Deal Size is a dimension by which we will measure stage to stage conversions and stage cycle times. Values are in USD.

1. Jumbo- any opportunity equal or greater than USD 100,000.
1. Big- any opportunity between USD 25,000 and USD 99,999.99.
1. Medium- any opportunity between USD 5,000 and USD 24,999.99.
1. Small- any opportunity below USD 5,000.

### Opportunity Stages

Also see the [Customer Lifecycle](https://about.gitlab.com/handbook/business-ops/customer-lifecycle/)

**00-Pre Opportunity**- This stage should be used when an opportunity does not meet our opportunity criteria. However, there is a potential for business, and it should be tracked for possible business.
* What to Complete in This Stage:
  * Schedule discovery call with prospect to determine if there is an opportunity to pursue; or
  * If there is no opportunity then the stage would be updated to 0-Unqualified.

**0-Pending Acceptance**: This is the initial stage once an opportunity is created.
* What to Complete in This Stage:
  * For BDR or SDR sourced opportunities, the opportunity meets [Sales Accepted Opportunity criteria](/handbook/marketing/marketing-sales-development/sdr/#criteria-for-sales-accepted-opportunity-sao).
  * The BDR or SDR has scheduled a call via Google Calendar, sent invites, created an event on the account object, named the event: Gitlab Introductory Meeting - {{Account Name}}
  * Once it is confirmed that the opportunity meets our Sales Accepted Opportunity criteria, the SAL or AE should move the opportunity to the next stage. The date the opportunity moves from this to the next stage in the sales cycle will populate the `Sales Accepted Date` field on the opportunity record.
  * If the details on the opportunity do not meet our Sales Accepted Opportunity criteria, the SAL or AE should move the opportunity to an `8-Unqualified` stage (this is the only time an opportunity can move into `8-Unqualified` stage).

**1-Discovery**: Uncover as much intelligence about the project as you can, which will be confirmed at later stages throughout the sales cycle.
* What to Complete in This Stage:
  * Uncover *Need to Buy, Internal Champion, Decision Making Process, Decision Criteria, Funding*, and *Business Drivers*
  * Send Plan Letter/Recap Email to Attendees- [Example](https://docs.google.com/document/d/16Gurj_MVREmKoqXTdB1F0OQ3eyq1gzbTNU8LNHHuoEM/edit)
  * Scheduled Scoping Call
  * Begin filling out [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification)
  * Should the opportunity progress from `1-Discovery` to the next stage (not 7-Closed Lost or 9-Duplicate), it will be considered a `Sales Qualified Opportunity`. The following values are entered once the opportunity progresses from this stage:
     * `Sales Qualified` is True.
     * `Sales Qualified Date` is the date the opportunity moves from this stage to the next open or won stage.
     * `Initial IACV` captures the value in the `Incremental ACV` field. `Initial IACV` is a snapshot field that will not change, even when the `Incremental ACV` field is updated and will be used for `Deal Size` analysis.

**2-Scoping**: Uncover business challenges/objectives, the competitive landscape, realizing fit.
* What to Complete in This Stage:
  * Uncover *Business Value Assessment, Competitive Awareness, Solution Fit*
  * Confirm *Need to Buy, Internal Champion*
  * Complete a Demo (Optional)
  * Schedule a Technical Evaluation Call
  * Confirm and collect new [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification) information.

**3-Technical Evaluation**: Confirming technical requirements. A proof-of-concept (POC) might occur at this stage. This is also the stage to confirm information before a proposal is delivered.
* What to Complete in This Stage:
  *  Define POC Scope and Plan (if applicable)
  *  Confirm *Technical Requirements, POC Scope, Business Value Assessment, Competitive Awareness, Decision Making Process, Decision Criteria, Funding, Solution Fit*
  *  Confirm *Technical Win/POC Success*
  *  Confirm and collect new [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification) information.

**4-Proposal**: Business and technical challenges and been uncovered and resolved. A proposal is drafted and delivered to the prospect.
* What to Complete in This Stage:
  * Uncover Bill to/Sold to information; Billing Details, Vendor Registration Process
  * Deliver formal contract to the prospect
  * An MSA may be delivered separately
  * Clear understanding of purchase/contract review process and a close plan (actions to be taken, named of people to complete actions and dates for each action) documented and attached to the opportunity.
  * Confirmation on who needs to sign off and when.

**5-Negotiating**: The prospect or customer has received the proposal and is in contract negotiations.
* What to Complete in This Stage:
  * Agreement on business terms

**6-Closed Won**: Congratulations!! The terms have been agreed to by both parties.
* What to Complete in This Stage:
  * Received signed order form from the prospect or customer, which signals agreement of all pricing and legal terms
  * All relevant documents, MSA, PO, and other forms uploaded to SFDC in the Notes and Attachments section of the opportunity record
  * EULA has been accepted by end-user organization (if applicable)
  * Subscription created in Zuora
  * Submit the opportunity for approval
  * Opportunity has been approved
  * Update Reason for Closed Won and add any pertinent notes
  * Introduce Customer Success/Account Management (if applicable)
  * Set a calendar reminder for 30 day follow up
  * If applicable, initiate Customer Onboarding or Premium Support onboarding

**7-Closed Lost**: An opportunity was lost and the prospect/customer has decided not to pursue the purchase of GitLab.
* What to Complete in This Stage:
  * Select all applicable Closed Lost Reason
  * In the Closed Lost Details, enter as much detail as you can as to why we lost the deal. For example:
     * If they selected a competitor, why? Was it due to features or pricing?
     * If decided not to move forward with a project, what were the reasons? Did they not understand the value? Was there not a compelling event or reason? 
     * Again, please be as thorugh as you can as this information will prove valuable as we learn from these experiences.
  * Please note that for new business deals where the opportunity is with a Large/Strategic account OR the Incremental Annual Contract Value (IACV) is equal or greater than USD 12,000, then a notification will be sent to the [#lost-deals](https://gitlab.slack.com/messages/C8RP2BBA7) Slack channel. 
  * Uncover a time for follow up (incumbent solution contract expiration date)

**8-Unqualified**: An opportunity was never qualified.
* What to Complete in This Stage:
  * Update Reason for Closed Lost and add any pertinent notes as to why the opportunity was not qualified.
  * A notification will be sent to BDR or SDR Team Lead and a feedback session should be scheduled between AE and Team Lead.

**9-Duplicate**: A duplicate opportunity exists in the system.

#### Opportunity Stage Movement Considerations
Note that once you qualify an opportunity via our standard qualification process, you cannot revert an opportunity back to the following stages: 00-Pre Opportunity, 0-Pre AE Qualified, or 8-Unqualified. If you need to revert an opportunity you've previously qualified to one of these stages, please contact Sales Operations and we can determine why the opportunity (once qualified) is no longer qualified.

### Capturing "MEDDPIC" Questions for Deeper Qualification

MEDDPIC is a proven sales methodology used for complex sales process into enterprise organizations. These questions should be answered in the "1-Discovery" Stage of the sales process. These questions will appear in the MEDDPIC Section of the Opportunity layout and will be required for all new business opportunities that are greater than $25,000 IACV.

* (M) Metrics: What is the economic impact of the solution?
  * Top line metrics include quicker time to market, higher quality- improvements towards sales and revenue.
  * Bottom line metrics include reduction in operating costs.
* (E) Economic Buyer: Who has profit/loss responsibility for this solution?
  * Economic Buyer (EB) has the power to spend.
  * Check with the EB on sponsorship, selection criteria.
* (D) Decision Criteria: Understand what is driving the decision?
  * What is the Technical Decision Criteria (TDC)?
  * What is the Business Decision Criteria (BDC)?
  * Check with both the Champion and the EB.
* (D) Decision Process: What will the decision-making process look like? Who is involved? What is their criteria for selection?
  *  What is the Technical Decision Process (TDM)?
  *  What is the Business Process (TBM)?
* (P) Purchasing Process: What does the purchasing process entail?
  * Is there a legal team involved in contract negotiations?
  * What does a purchase of this size look like?
  * Has a purchase of this size happened before?
* (I) Identify Pain: What are the primary objectives?
  * Is there a compelling event?
  * Remember, nobody buys until there is pain.
* (C) Champion: Who will sell on your behalf inside the company?
  * Is this person qualified to be your Champion?
  * Have you tested this person as your Champion?
  * Does this person know he/she is your Champion?

### Tracking Proof of Concepts (POCs)

In Stage 3-Technical Evaluation, a prospect or customer may engage in a proof of concept as part of the evaluation. If they decide to engage, please fill in the following information, as we will use this to track existing opportunities currently engaged in a POC, as well as win rates for customers who have engaged in a POC vs. those that do not.

1. In the opportunity, go to the 3-Technical Evaluation Section.
1. In the POC Status field, you can enter the status of the POC. The options are Not Started, Planning, In Progress, Completed, or Cancelled.
1. Enter the POC Start and End Dates.
1. Enter any POC Notes.
1. Enter the POC Success Criteria, ie how does the prospect/customer plan to determine whether the POC was successful or not.

### Submitting Weekly Forecasts

Each week, SALs, AEs, AMs, and Channel Managers are responsible for submitting a weekly forecast for the current quarter in InsightSquared. Regional forecasts are due by **Monday 10 AM Pacific Time**. The regional numbers will be documented and shared on the weekly forecast call.

There are two categories that opportunities will fall into when submitted for a forecast:

* **Commit**: refers to opportunities that are either currently in `5-Negotiating` stage or in an earlier stages with a high probability (90%) of closing in the period. You should be very confident that these deals will close in this period.
* **Best Case**: refers to opportunities that are either currently in `4-Proposal` stage or in an earlier stage with a moderate (50%) probability of closing in the period. You do not have to be as accurate as the Commit forecast, but you should still practice discretion as you add opportunities to Best Case forecast.

Please use these terms correctly and don't introduce other words. Apart from the above the company uses two other terms:

* **Plan**: Our yearly operating plan that is our promise to the board. The IACV number has a 50% of being too low and 50% chance of being too high but we should always hit the TCV - Opex number.
* **Forecast**: Our latest estimate that has a 50% of being too low and 50% chance of being too high.

To submit a forecast, complete the following steps:

1. Log into InsightSquared and go to `Settings` on the top right.
1. Select `Forecast Entry`.
1. Go to the month you'd like to forecast.
1. Click on the magnifying glass icon next to the `Commit` field.
1. Select the opportunities you'd like to add to your commit for that month. You can include opportunities up to three (3) months out.
1. As you select opportunities, you will see the total value field increase. Once you're done, click on the `Use This Number` field.
1. Click on the magnifying glass icon next to the `Best Case` field for that same month.
1. Repeat Steps 5 and 6.
1. Then repeat this for all of the months available (three months).
1. Note that you should not select the same opportunity for both a `Commit` and a `Best Case`, otherwise this will cause duplicate entries into your forecast.

If you are a Regional Director or oversee a team of sales reps, you can overwrite your entire team's forecast or that of an individual team member:

1. Log into InsightSquared and go to `Settings` on the top right.
1. Select `Forecast Entry`.
1. If you want to overwrite your entire team's forecast, go to the very top line of the forecast hierarchy and enter the value that you believe will close. The box should include your title or role (eg, Regional Director- EMEA).
1. If you want to overwrite an individual team member's forecast, go to that team member's line in the forecast hierarchy and enter the value you believe will close.
1. Enter the Commit and Best Case for each of the months that you would like to overwrite.
1. Click `Save`.

The final forecast will appear in the [Regional Director Forecast by Team](https://clients.insightsquared.com/reports/manual_forecast/by_team/?sort=name%7C-forecast%7Cgoals&is_expanded=MTIzLDE0MCw4xII5Miw5OMSDMTbEjzTEj8SOxIDEhjnEkTEwxJE4xI44xJPEgMSLxJrEoDDEhsSaxI45xIY4xJk0McSDNMSiNMSVNcSCxK0sxLDEtzksNcSGN8SexKIsNyzEizPEjjTFgsSy&ignore_subgoals=0&legendchecked=UGlwZWxpbmUsQm9va8SFZ3MgR29hbA%3D%3D&date_tab=this_quarter&legendunchecked=UGlwZWxpbmU%3D&is_checked=OTM%3D&second_date=monthly_view&logic_selector=OR&f_type=1) report in the [Sales Forecast](https://clients.insightsquared.com/reports/dashboard/db117617/) dashboard in InsightSquared.


### Escalation to Support

During the sales cycle potential customers that have questions that are not within the scope of sales can have their queries escalated in different ways depending on the account size:

1. For Large accounts that will have a dedicated Solutions Architect, [engage the SA](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect) so that they can triage and develop the request.
1. For questions that you think technical staff can answer in < 10 minutes, see the [internal support](/handbook/support/#internal-support-for-gitlab-team-members) section of the support handbook.
1. If a potential customer has already asked you a question, forward a customer question via email to the **support-trials** email address. - It's important the email is **forwarded** and not CC'd to avoid additional changes required once the support request is lodged.

### How Support Level in Salesforce is Established

Once a prospect becomes a customer, depending on the product purchased, they will be assigned a Support Level (Account) in Salesforce:

#### GitLab EE
1. Premium: Enterprise Edition Ultimate customers; Enterprise Edition Premium customers; any Enterprise Edition Starter customer who has purchased the Premium Support add on
1. Basic: any Enterprise Edition Starter customer without the Premium Support add on
1. Custom: any customer on Standard or Plus legacy package

#### GitLab.com
1. Gold: Gitlab.com Gold Plan
1. Silver: Gitlab.com Silver Plan
1. Bronze: Gitlab.com Bronze Plan

If a customer does not renew their plan, their account will be transitioned to an "Expired" status.

### Contributing to EE Direction

Being in a customer facing role, salespeople are expected to contribute to [GitLab Direction](https://about.gitlab.com/direction/).  Each day we talk to customers and prospects we have the opportunity to hear of a feature they find valuable or to give feedback (positive and constructive) to an EE feature that there is an issue for.
When you hear of feedback or you personally have feedback, we encourage you to comment within the issue, if one exists, or create your own issue on our [EE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues). Checking the [GitLab Direction](https://about.gitlab.com/direction/) page and the [EE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues) should happen throughout the week.

When you have an organization that is interested in an EE feature and you have commented in the issue and added a link to the account in salesforce, please follow the process outlined on the [product handbook](/handbook/product/#example-a-customer-has-a-feature-request) to arrange a call with the product manager and account to further discuss need of feature request.

### Export Control Classification, and Countries We Do Not Do Business In

GitLab's Export Control Classification (or ECCN) is 5D002.c.1 with CCATS number G163509.
This means that GitLab source code can be exported and re-exported under the authority of license exception TSU of section [740.13(e)](https://www.bis.doc.gov/index.php/forms-documents/doc_view/986-740) of the export administration regulations (EAR).

Per [740.13(e)(2)(ii)](https://www.bis.doc.gov/index.php/forms-documents/doc_view/986-740) of the EAR, there are restrictions on "Any knowing export or reexport
to a country listed in [Country Group E:1 in Supplement No. 1 to part 740 of the EAR](https://www.bis.doc.gov/index.php/forms-documents/doc_download/944-740-supp-1)".

As a consequence of this classification, we currently do not do business in Iran, Sudan (excluding South Sudan), Syria, North Korea, and Cuba.
