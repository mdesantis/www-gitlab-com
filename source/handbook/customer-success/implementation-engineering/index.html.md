---
layout: markdown_page
title: "Implementation Engineering"
---
# Implementation Engineering Handbook
{:.no_toc}

The Implementation Engineering group at GitLab is a part of the [Customer Success](/handbook/customer-success) department. Implementation Engineers have the goal to optimize organization's adoption of GitLab through our implementation services, designed to enable other necessary systems in your environment so that customers can move code from idea to production.

## On this page
{:.no_toc}

- TOC
{:toc}

## Role & Responsiblities
See the [Implementation Engineer job description](/jobs/implementation-engineer/)

### Statement of Work creation

The GitLab Implementation Engineering group is responsible for both maintaining the [GitLab SOW Template](https://docs.google.com/document/d/1X8_EiX8kgJdpaVlydbTJg5pn4RXeDvYOIyok2G1A69I/edit) (internal link) as well as the production of new Statements of Work for customer proposals.

To obtain a Statement of Work, open a new SOW issue using the template on the [Professional Services project](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) (internal link). We prefer customers to mark up our agreement and SOW template if they request changes. If they require the use of their services terms or SOW, please contact the Implementation Engineering group.

### Services Delivery

#### Implementation Plan

Each services engagement will include an Implementation Plan compiled based on the Statement of Work and other discussions the Implementation Engineering group has with the customer.  The Implementation Engineering group also maintains the SOW template located [at this internal link](https://docs.google.com/document/d/1ohZtqR1mMZYWAOD7PBVc1xZIMGpzHNgccRxVZ9Nzirg/edit).

## Professional Services Offerings

### Discovery Engagement 

During a Discovery Engagement, the Implementation Engineering group will engage with a customer for a set period - typically one week - for a set price.  The Implementation Engineering group will consult with the customer and provide advice such as:

* Onsite discovery of short, mid and long-term goals for successful deployment of Gitlab in High-Availability mode. 
* Technical deep-dive into existing infrastructure (network topology, VM environment, inter-datacenter connectivity, security constraints). 
* Scope planning for short and mid-term goals to deliver Gitlab to their internal customers relative to their enterprise SLAs.

### Training & Education

Below are the main training courses that we currently offer.  Below find a description of each and an (internal) link to the slides that go along with the training.

* [GitLab with Git Basics](https://docs.google.com/presentation/d/1R_G9YPM3aS-3lUdAm4hu9jfP7xLYjqm5zzgh7AYPyw8/edit): This course is for users that are new to Git and GitLab.  The course walks through the basics of Git (commit, branching, trees, etc.) as well as how those basics work in the context of both the GitLab user interface and interacting with GitLab through the CLI.
* [GitLab Admin Training](https://docs.google.com/presentation/d/1EKOEcQ_8qE0rQADmbzBKVoeMm7eEivkb_S_2rq4TCAU/edit): This course is for new GitLab administrators.  The course covers topics about installing, configuring and maintaining a GitLab instance.
* [GitLab CI/CD Training](https://docs.google.com/presentation/d/1kx9P3n5AOKSXM9x1TWMOsjm7wVfrn12bZOLlJsYkvxo/edit): This course is useful to developers, administrators and DevOps professionals alike.  Topics covered include using GitLab CI/CD with your projects, the .gitlab-ci.yml file and the various ways GitLab can be used as a Complete DevOps platform for the entire SDLC.

### Migration Services

As part of services engagement, we offer migration services from a customer's current systems.  These services include consulting with the customer's technical staff about the best way to migrate not only the customer's source code but also their entire SDLC to the GitLab platform.  We may implement the migration in a phased approach or a replacement approach depending on the customer's comfort level and desires around adoption.

This can include:
* Importing your projects from GitHub, Bitbucket, GitLab.com, FogBugz and SVN into GitLab
* Migrating from SVN: Convert a SVN repository to Git and GitLab

In all cases, a GitLab Adoption Plan will be created with the customer to outline the goals of the migration and adoption, as well as any plans that involve customer or GitLab Professional Services actions.

### Integration Services

The GitLab Implementation Engineering group will also work with a customer's technical team to come up with an Integration Plan for the GitLab implementation.  Typically part of the overall Implementation Plan, this Integration Plan will consider a customer's existing systems and corporate policies in regards to:

* Integration with LDAP/AD or other OAuth services for authentication
* Integration to Jira, Jenkins, Redmine, Mattermost
* Custom Integrations
   - Automation
   - API: Automate GitLab via a simple and powerful API.
   - GitLab Webhooks: Let GitLab notify another system when new code is pushed

### Specialized Training

#### Train the Trainer

The GitLab Train the Trainer program is a set of workshops designed to enable super-users at customer organizations to be able to train their team on Git and GitLab best practices.

#### GitLab Certification Program

The Implementation Engineering group will be rolling out a GitLab Certification Program in the second quarter of 2018.  To view the progress of this initiative, see [this internal issue](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/46)

## How to work with/in the Implementation Engineering group

### Contacting & Scheduling

At GitLab, Implementation Engineering is part of the [Customer Success department](/handbook/customer-success).  As such, you can engage with Implementation Engineering by following the guidelines for [engaging with any Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).  This process ensures that the Customer Success department as a whole can understand the inbound needs of the account executive and our customers.

For scheduling specific customer engagements, we currently are slotting implementations while our group grows to support the demand for services.  If you have a concern about scheduling the engagement, this should be discussed at the Discovery phase.  In no case should you commit to dates before receipt of agreements, P.O., etc.

### Selling Professional Services

#### How much do Professional Services cost?

Our customer's needs vary greatly depending on their experience with GitLab, their current business practices and where they fall on their journey along the DevOps Maturity Model.

As such, our Implementation Engineering group has some package options that give a general [ROM](https://cso.nasa.gov/content/roms) cost figure for the scope of engagement but can customize those packages based on the customer's need.  The best way to elicit this would be a discovery call with the customer.  You also can see some of the base packages at [this internal link](https://docs.google.com/document/d/1bbEamRV3yz1VO2Ze_0qvvCk8QikpmB8eC1BVIpPQxGY/edit).

Any discounts will need approval from Director of Customer Success.

#### What are our daily or hourly rates?

We do not currently have an hourly or daily rate.  Nor do we plan to have an hourly rate.  Just as with GitLab support, the mission of our Implementation Engineering group is not to bill hours, but to achieve success for our Customers.  So just as we don't offer support by the call or by the hour, we do not offer Professional Services by the day or the hour.

In the future, we may have a daily rate or a daily rate for on-site support.  However, we currently do not for the same reasons listed above.

#### What if the customer only wants training?

If the customer is an EE customer, we can offer training.  However, training will need to be scoped out by the customer success department before quoting a price.  The account executive will also be required to provide the use case for the need for just training.  Example use case might be, under license utilization and we need to prevent churn, help expand usage into additional groups and other business units.

#### What options exist for CE Customers?

GitLab CE is ideal for personal projects or small groups with minimal user management and workflow control needs.  Because these groups don't typically need a lot of focus on scaled implementation or training, we currently do not offer implementation, integration or training services to our CE customers.  Many of our [partners](https://about.gitlab.com/resellers/) offer such services.  However, we find that many customers who have been running a CE instance and are looking to transition to a scaled implementation of GitLab may require a [Discovery Engagement](#discovery-engagement) to determine the customer's long-term needs.

If a customer is upgrading from CE to EE, Professional Services should be engaged to scope out the requirements for the transition if the customer will need services in the transition. 

### Contacting Implementation Engineering

To contact the Implementation Engineering group, the best way is to follow the guidelines for [Engaging a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).  

You can also reach the group via the [#professionalservices Slack Channel](https://gitlab.slack.com/messages/C7DAU4WG0/).

### Implementation Engineering Issue Board

The [Implementation Engineering Issue Board is available here](https://gitlab.com/gitlab-com/customer-success/professional-services/boards).  This board contains everything that the group is working on - from strategic initiatives to [SOW writing](#statement-of-work-creation), all group activity is available here.

#### SOW Issues

When requesting a SOW, Account Executives or Implementation Engineering group members should use the SOW issue template, which automatically shows the required information as well as labels the issue appropriately.

#### Strategic Initiatives
Strategic Initiatives are undertaken by the Implementation Engineering group to leverage team member time in a given area of the Customer Success Department.  This can include increased documentation, better training resources or program development.

**Current Strategic Initiatives**
* [Improvments to GitLab University](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/12)
* [Managed Trials](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/15)
* [Data gathering around GitLab CI/CD usage & comparitaive analysis](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/39)
* [GitLab Certifications Program](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/46)